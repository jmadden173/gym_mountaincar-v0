import os
import neat
import gym
import numpy as np
import time

env = gym.make('MountainCar-v0')

env.reset()

print("\naction space: {0!r}".format(env.action_space))
print("observation space: {0!r}\n".format(env.observation_space))


def checkOutput(action):
    if (0 <= action[0] <= 2): return True
    return False


def convertOutput(action):
    for i in range(len(action)):
        # Round to the closest whole number
        # Convert to int
        action[i] = int(round(action[i],0))


def simulate(genomes, config):
    for genome_id, genome in genomes:
        observation = env.reset()

        done = False
        steps = 0
        tmpfitness = 0.0

        net = neat.nn.FeedForwardNetwork.create(genome, config)

        while (not done) and (steps < 2000):
            #print("Observation: {}".format(observation))
            output = net.activate((observation[0], observation[1],))
            if checkOutput(output):
                convertOutput(output)
                if (output[0] == 1): output[0] = 2
                #print("Output: {}".format(output))
                observation, reward, done, info = env.step(output[0])
                #env.render()
                tmpfitness += reward
                #print("Got Correct Output")
            else:
                tmpfitness -= 2
            steps += 1

        genome.fitness = tmpfitness
        #print("Finished Genome: {}".format(genome_id))


def run(config_file):
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                        neat.DefaultSpeciesSet, neat.DefaultStagnation,
                        config_file)

    # Creates a populations
    p = neat.Population(config)

    # Add stdout for writing to the terminal
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)
    p.add_reporter(neat.Checkpointer(5))

    # Run for 300 generations
    winner = p.run(simulate, 10)

    print("Saving Best Genome")
    winner.write_config("genome_winner", config)

    # Display winnning genome
    print('\nBest genome:\n{!s})'.format(winner))

    print('\nTesting Best Genome:\n')
    observation = env.reset()
    winner_net = neat.nn.FeedForwardNetwork.create(winner, config)
    while True:

        action = winner_net.activate((observation[0], observation[1],))
        convertOutput(action)
        observation, reward, done, info = env.step(action[0])
        env.render()
        if done:
            env.reset()
            print("\n\n------------------------- Finished -------------------------\n\n")
        os.system("PAUSE")


    env.close()

if __name__ == "__main__":
    # Path of the config file in the
    # working directory
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, 'config')
    run(config_path)
